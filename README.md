# classification-with-style



## Getting started

- Define a datastructure to encode the various graph levels
- How to determine the links between words, sentences, paragraphs… ?
- How to build a gold (-> wikipédia ?) ?

## Goals

- Identify the reasons why a text is classified as one domain or another
- Understand the links between micro-information (at the word level) and
  discursive genre (at the text level)

